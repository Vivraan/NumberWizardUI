﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
	public void LoadLevel(string name)
	{
		Debug.Log($"Level load requested: {name}");
		SceneManager.LoadScene(name);
	}

	public void RequestQuit()
	{
		Debug.Log("I suck at life and want to quit.");
		Application.Quit();
	}
}