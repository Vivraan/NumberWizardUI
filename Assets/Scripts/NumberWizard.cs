﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NumberWizard : MonoBehaviour
{
	[SerializeField] private Canvas canvas;
	[SerializeField] private Color highlightColor;
	[SerializeField] private int maxGuesses;
	private Text flavourText;
	private Text numberText;
	private int remainingGuesses;
	private int max;
	private int min;
	private int previousGuess;
	private int currentGuess;
	private string highlightColorHex;

	// Use this for initialization
	void Start()
	{
		max = 1000;
		min = 1;
		previousGuess = int.MinValue;
		remainingGuesses = maxGuesses;

		highlightColorHex = "#" + ColorUtility.ToHtmlStringRGBA(highlightColor);
		flavourText = canvas.transform.Find("Flavour").GetComponent<Text>();
		numberText = canvas.transform.Find("Number").GetComponent<Text>();

		NextGuess();
		max++;
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			GuessHigher();
		}
		else if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			GuessLower();
		}
		else if (Input.GetKeyDown(KeyCode.Return))
		{
			SceneManager.LoadScene("Lose");
		}
	}

	public void GuessHigher()
	{
		min = currentGuess;
		NextGuess();
	}

	public void GuessLower()
	{
		max = currentGuess;
		NextGuess();
	}

	private void NextGuess()
	{
		//currentGuess = (max - min) / 2 + min);
		currentGuess = Random.Range(min, max);

		if (previousGuess == currentGuess)
		{
			flavourText.text = ($"Come on! <color={highlightColorHex}>{currentGuess}</color> " +
				"<i>has</i> to be the number! Check your statements yourself...\n\n" +
				"[Just hit Return you smarty-pants!]");

			numberText.text = $"<color={highlightColorHex}><b><i>{currentGuess}</i></b></color>";
		}
		else
		{
			previousGuess = currentGuess;
			flavourText.text = ($"Is it higher or lower than <color={highlightColorHex}>{currentGuess}</color>?\n\n" +
				"[Up-arrow for higher, Down-arrow for lower, Return-key for equals. Or just click those buttons. Yeah.]");

			numberText.text = $"<color={highlightColorHex}>{currentGuess}</color>";
		}

		remainingGuesses--;

		if (remainingGuesses <= 0)
		{
			SceneManager.LoadScene(previousGuess == currentGuess? "Lose" : "Win");
		}
	}
}
